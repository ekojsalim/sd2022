import streamlit as st

from bertopic import BERTopic
from transformers import pipeline
from huggingface_hub import hf_hub_download

@st.experimental_singleton
def get_topic_model():
    fpath = hf_hub_download(repo_id="ekojs/satdat2022", filename="topic_model")
    topic_model = BERTopic.load(fpath, embedding_model="firqaaa/indo-sentence-bert-base")
    return topic_model

@st.experimental_singleton
def get_sa_model():
    model = pipeline(
        "sentiment-analysis",
        model="ekojs/satdata-sentiment-tuned",
        tokenizer="ekojs/satdata-sentiment-tuned",
        device=0
    )
    return model

@st.experimental_memo
def get_tweets():
    df = pd.read_csv("./data/tweets.csv")[["text", "sentiment", "topic"]]
    return df