import streamlit as st

@st.experimental_memo
def get_topic_viz(tm):
    return tm.visualize_topics()