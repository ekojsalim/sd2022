import pandas as pd

import streamlit as st
import plotly.io as pio
import plotly.express as px

import preprocessor as tweet_cleaner

import snscrape.modules.twitter as twtscraper

from lib import data

def get_tweets(count=20, terms=""):
    tweets = []
    for i, twt in enumerate(twtscraper.TwitterSearchScraper(f"bpjs {terms} lang:id").get_items()):
        if i >= count:
            break
        tweets.append([twt.date, twt.content, twt.url])

    tweets = pd.DataFrame(tweets, columns=["date", "text", "url"])
    
    return tweets

def get_sentiment(tweets_df):
    preds = sa_model(tweets_df.cleaned.to_list())
    preds = [pred["label"] for pred in preds]    
    return preds

def get_topic(tweets_df):
    topics, probs = topic_model.transform(tweets_df.cleaned.to_list())
    return [topic_model.topic_labels_[t] for t in topics]

st.markdown("# Live Scrape & Analysis")

st.write("Tweet terakhir yang mengandung kata 'bpjs' akan di-scrape. Anda bisa menambahkan kata kunci tambahan dan mengatur jumlah tweet yang diambil.")

tm = data.get_topic_model()

c1, c2 = st.columns([1, 3])

tweet_count = c1.number_input("Jumlah tweet diinginkan", min_value=10, max_value=500, step=10, value=50)
terms = c2.text_input("Search term tambahan", value="", placeholder="pelayanan (Optional)")

if st.button("Analyze"):
    with st.spinner("Meng-load model..."):
        topic_model = data.get_topic_model()
        sa_model = data.get_sa_model()
    
    with st.spinner("Mengambil tweet..."):
        tweets_df = get_tweets(tweet_count, terms)
        
    with st.spinner("Menganalisis tweet..."):
        tweets_df["cleaned"] = tweets_df.text.apply(tweet_cleaner.clean)        
        tweets_df["sentiment"] = get_sentiment(tweets_df)
        tweets_df["topic"] = get_topic(tweets_df)
        
    st.markdown("""## Analisis""")
    st.write(f"{len(tweets_df)} berhasil diambil. Berikut adalah analisis sentimen yang terdeteksi:")

    col1, col2, col3 = st.columns(3)
    col1.metric("Negatif", sum(tweets_df.sentiment == "negative"))
    col2.metric("Netral", sum(tweets_df.sentiment == "neutral"))
    col3.metric("Positif", sum(tweets_df.sentiment == "positive"))
    
    st.markdown("---")
    
    st.markdown("Berikut adalah topik-topik yang terdeteksi:")
    
    twts_wo_outlier = tweets_df[tweets_df.topic != "-1_bpjs_di_yg_dan"]
            
    count_df = twts_wo_outlier.groupby("topic").topic.count().to_frame()
    count_df.columns = ["jumlah"]
    count_df["contoh"] = [twts_wo_outlier[twts_wo_outlier["topic"] == v].iloc[0].text for v in count_df.index]
    
    st.dataframe(count_df)
    
    hist = px.histogram(twts_wo_outlier, x="topic", color="sentiment",
                       color_discrete_map={
                           "neutral": '#636EFA',
                           "negative": '#EF553B',
                           "positive":  '#00CC96'
                       })
    st.write(hist)    
            
    st.markdown("---")
    
    st.write("Berikut adalah tabel semua tweet yang terkumpul:")
        
    st.dataframe(tweets_df.drop(columns=["cleaned"]), use_container_width=True)