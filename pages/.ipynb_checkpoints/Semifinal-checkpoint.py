import streamlit as st
from lib import data
import plotly.io as pio

st.markdown("# Semifinal")

# tm = data.get_topic_model()
# ti = tm.get_topic_info()
# st.write("Informasi mengenai topik yang dihasilkan oleh BERTopic ditunjukkan oleh tabel berikut.", ti)

# Line
st.markdown("## Topic Over Time")
st.write("Perubahan representasi topik yang dihasilkan melalui *Dynamic Topic Modelling* seiring waktu dapat dilihat dengan cara mengarahkan kursor ke titik-titik pada line chart berikut.")
st.write(pio.read_json("./viz/dynamic_time_viz.json"))

# Distance Map
st.markdown("## Intertopic Distance Map")
st.write("Visualisasi jarak topik-topik dalam dua dimensi. Topik-topik yang mirip akan berada dekat satu sama lain.")
st.write(pio.read_json("./viz/topics_viz.json"))

# Hierarchy
st.markdown("## Hierarchial Clustering")
st.write("Topik-topik yang sudah dihasilkan bisa direduksi secara hierarkis. Skema struktur hasil penggabungan topik-topik yang saling berkaitan dapat dilihat pada visualisasi berikut.")
st.write(pio.read_json("./viz/hierarchical_label.json"))

# Heatmap
st.markdown("## Similarity Matrix")
st.write("Matriks berikut menunjukkan seberapa mirip suatu topik dengan topik yang lain dengan memanfaatkan c-TF-IDF dan embedding.")
st.write(pio.read_json("./viz/heatmap_label.json"))

# Bar chart
st.markdown("## Topic Word Scores")
st.write("Setiap topik memiliki beberapa kata kunci yang berkaitan erat dengan topik tersebut. Berikut adalah kata kunci yang paling sering muncul untuk setiap setiap topik.")
st.write(pio.read_json("./viz/barchart_label.json"))

