import streamlit as st

st.set_page_config(
    page_title="t-SNE SatriaData 2022",
    page_icon="👋",
)

st.write("# SatriaData Demo oleh t-SNE")


st.markdown(
    """
    Pergi ke halaman 'Live' untuk meng-scrape dan menganalisis tweet-tweet terakhir tentang BPJS. Selain itu, halaman 'Semifinal' mengandung beberapa visualisasi interaktif mengenai permasalahan semifinal.
"""
)
